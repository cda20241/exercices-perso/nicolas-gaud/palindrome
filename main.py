STRING_TO_CHECK = input("Entrez un mot ou une phrase : ").lower()

def checkpalindrome(str):
    """"
    :param: Un mot ou une phrase entré par l'utilisateur
    :return: True si le mot ou la phrase est un palindrome, False sinon
    """
    charlist = []
    ispalindrome = False
    if len(str) > 1:
        for char in str:
            if char != " ":
                charlist.append(char)
        if charlist[0] == charlist[len(charlist)-1]:
            ispalindrome = True
        for i in range(1, int(len(charlist) // 2)):
            if charlist[i] != charlist[len(charlist) - (i + 1)]:
                ispalindrome = False
    return ispalindrome


print('"' + STRING_TO_CHECK + '" est un palindrome') if checkpalindrome(STRING_TO_CHECK) \
    else print('"' + STRING_TO_CHECK + '" n\'est pas un palindrome')